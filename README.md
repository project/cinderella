# Cinderella module for Drupal

This module provides a communication service between Drupal and Cinderella.

## Cinderella

Cinderella is asyncronous PHP daemon for scheduling and running tasks.

See: https://github.com/coldfrontlabs/cinderella
