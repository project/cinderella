<?php

namespace Drupal\cinderella;

use Drupal\wsdata\WSDataService;
use Drupal\Core\State\State;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

class Cinderella implements EventSubscriberInterface {
  protected $wsdata;
  protected $state;

  public function __construct(WSDataService $wsdata, State $state) {
    $this->wsdata = $wsdata;
    $this->state = $state;
  }

  public function refreshSchedule() {
    return $this->wsdata->call('cinderella_refresh_schedule') !== FALSE;
  }

  public function connected() {
    return $this->status() !== FALSE;
  }

  public function status() {
    return $this->wsdata->call('cinderella_status');
  }

  public function runTasks(Array $tasks, HttpTask $resolve) {
    $data = [
      'type' => 'task_runner',
      'tasks' => [],
      'resolve' => $resolve->toArray(),
    ];
    foreach ($tasks as $task) {
      $data['tasks'][] = $task->toArray();
    }

    return $this->wsdata->call('cinderella_task', NULL, [], $data);
  }

  public function queuedTask($queueid, HttpTask $task, HttpTask $resolve) {
    $data = [
      'type' => 'queued_task',
      'task' => [
        'type' => 'task_runner',
        'tasks' => [$task->toArray()],
        'resolve' => $resolve->toArray(),
      ],
    ];
    return $this->wsdata->call('cinderella_task', NULL, [], $data);
  }

  public function queueRefreshSchedule() {
    $this->state->set('cinderella:refreshschedule', TRUE);
  }

  public function refreshScheduleIfRequired($event) {
    if ($this->state->get('cinderella:refreshschedule')) {
      $this->state->delete('cinderella:refreshschedule');
      $this->refreshSchedule();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::RESPONSE => 'refreshScheduleIfRequired',
    ];
  }
}
