<?php

namespace Drupal\cinderella;

use Drupal\Component\Serialization\Json;

Class HttpTask {
  public $id;
  public $url;
  public $data;
  public $method;
  public $headers;
  public $timeout;

  public function __construct($id, $url, $data = NULL, $method = 'GET', $headers = [], $timeout = 15, $maxsize = 10 * 1024 * 1024) {
    $this->id = $id;
    $this->url = $url;
    $this->data = $data;
    $this->headers = $headers;
    $this->timeout = $timeout;
    $this->maxsize = $maxsize;

    if (!in_array($method, ['GET', 'POST'])) {
      $method = 'GET';
    }
    $this->method = $method;
  }

  public function toArray() {
    return [
      'id' => $this->id,
      'type' => 'http_request',
      'url' => $this->url,
      'method' => $this->method,
      'body' => $this->data,
      'headers' => $this->headers,
      'timeout' => $this->timeout,
      'maxsize' => $this->maxsize,
    ];
  }
}